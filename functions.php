
<?php

// Functions for 4GoodHomeServices

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

// Add specific CSS class by filter
add_filter( 'body_class', 'my_class_names' );
function my_class_names( $classes ) {
	// add 'rd' and other class names to give us the specific rd theme
	$classes[] = 'rd';
	// return the $classes array
	return $classes;
}


?>
