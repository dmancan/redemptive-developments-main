<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rd-main
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KD8KTZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KD8KTZ');</script>
<!-- End Google Tag Manager -->


<div class="viewport">
	<main>

<!-- 
	################################################################################ 
	Navigation is here
	################################################################################
-->

	<nav class="navbar navbar-default rd">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand  hidden-xs hidden-sm" href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri();?>/assets/svg/logo.rd.01.svg" alt="logo.01">
	  			</a>
	  			<a class="navbar-brand  visible-xs visible-sm" href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri();?>/assets/svg/logo.rd.02.svg" alt="logo.01">
	  			</a>
	  		</div>
	  		
	  		<div class="collapse navbar-collapse" id="navigation">
		  		<?php /* Primary navigation */
				wp_nav_menu( array(
				  'menu' => 'Primary',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
				
	  		</div>
	  		
	  		<div class="btn-group tool-bar hidden-xs hidden-sm" role="group" aria-label="...">	
				<?php /* Seconardy navigation */
				wp_nav_menu( array(
				  'menu' => 'Secondary',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
			</div>
	  		
	  		
		</div>
	</nav>

	<div class="visible-xs visible-sm menu-mobile" role="navigation">
			<?php /* Mobile navigation */
				wp_nav_menu( array(
				  'menu' => 'Mobile',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav nav-pills nav-justified',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>

	</div>

	
