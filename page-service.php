<?php
/**
 * Template Name: Service Template 
 * The template for displaying services.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>
	
	
 <article class="para">
		<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>
		
		
		<?php if ( have_posts() ) : the_post(); ?>
		
			<div class="row">
				<div class="col-md-6">
					<h1><?php echo $post->post_title ?></h1>
					
					<?php if(get_field('sub_title_text')):?>
						<?php echo '<h2>'.get_field('sub_title_text').'</h2>'?>
					<?php endif; ?>
					
					<?php the_content(); ?>
				
				</div>
				
				
				<div class="col-md-6">
					<div class="well">
						<div class="row">
							<div class="col-xs-3">
								<img src="<?php echo get_template_directory_uri();?>/assets/svg/headset.01.svg">
							</div>
							<div class="col-xs-9">	
								<h3><?php if(get_field('contact_cta')):?> <?php the_field('contact_cta');?> <?php endif;?></h3>
								<h4><?php if(get_field('secondary_cta')):?> <?php the_field('secondary_cta');?> <?php endif;?></h4>
								
							</div>
						</div>
						<hr>
						
						<div class="well">
						<?php the_field('form_snippet');?>
						</div><!--end well-->
						
					</div><!--end well-->
				</div><!--end col-->
			</div><!--end row-->
	
	
		<hr>
		
		<?php if(have_rows('table_data')):?>
		
		
			<div class="row">
				<div class="col-md-6">
					<h4><?php if(get_field('table_title')):?> <?php the_field('table_title');?> <?php endif;?></h4>
					<table class="table table-striped">
						
						<tr>
							<th><?php if(get_field('table_column_1')):?> <?php the_field('table_column_1');?> <?php endif;?></th>
							<th><?php if(get_field('table_column_2')):?> <?php the_field('table_column_2');?> <?php endif;?></th>
							<th><?php if(get_field('table_column_3')):?> <?php the_field('table_column_3');?> <?php endif;?></th>
						</tr>
						
						
						<?php if(have_rows('table_data')): while(have_rows('table_data')): the_row();?>
						<tr>
							<td><?php if(get_sub_field('table_data_1')):?> <?php the_sub_field('table_data_1');?> <?php endif;?></td>
							<td><?php if(get_sub_field('table_data_2')):?> <?php the_sub_field('table_data_2');?> <?php endif;?></td>
							<td><strong><?php if(get_sub_field('table_data_3')):?> <?php the_sub_field('table_data_3');?> <?php endif;?><strong></td>
						</tr>
						<?php endwhile; endif;?>
				
					</table>
				</div>
				<div class="col-md-6">
					
					
					<h4><?php if(get_field('service_value_offering_header')):?> <?php the_field('service_value_offering_header');?> <?php endif;?></h4>
					<?php if(get_field('service_value_offering')):?> <?php the_field('service_value_offering');?> <?php endif;?>
				</div>
				
				

				
			</div><!--end row-->
			
		<?php endif; ?>
	
	
		<?php endif ?>
		

		
	</section>

	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
	
</article>

<?php get_footer(); ?>